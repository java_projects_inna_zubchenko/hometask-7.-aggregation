package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit {
    public SpecialDeposit(BigDecimal amount, int period){
        super(amount, period);
    }
    @Override
    public BigDecimal income(){
        BigDecimal finalSum = amount;
        for(int i = 1; i <= period; i++){
            finalSum = finalSum.add(finalSum.multiply(BigDecimal.valueOf(((double)i)/100.)));
        }
        return (finalSum.subtract(amount)).setScale(2, RoundingMode.HALF_DOWN);
    }
}
