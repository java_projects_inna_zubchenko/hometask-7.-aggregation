package com.epam.test.automation.java.practice7;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDeposit extends Deposit{
    public LongDeposit(BigDecimal amount, int period){
        super(amount, period);
    }
    @Override
    public BigDecimal income(){
        BigDecimal finalSum = amount;
        if(period>6){
        for(int i =6; i<period; i++){
            finalSum = finalSum.add(finalSum.multiply(BigDecimal.valueOf(0.15)));

        }
        return finalSum.subtract(amount).setScale(2, RoundingMode.HALF_DOWN);
        }
        else
            return finalSum.subtract(amount);
    }
}
