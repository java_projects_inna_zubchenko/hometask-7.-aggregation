package com.epam.test.automation.java.practice7;


import java.math.BigDecimal;

public class Client {
    private final Deposit[] deposits;
    public Client(){
        deposits = new Deposit[10];
    }
    public boolean addDeposit(Deposit deposit)
    {
        for (int i = 0; i < deposits.length; i++)
        {
            if (deposits[i] == null)
            {
                deposits[i] = deposit;
                return true;
            }
        }
        return false;
    }
    public BigDecimal totalIncome()
    {
        BigDecimal totalAmount = BigDecimal.valueOf(0);
        for (Deposit deposit : deposits) {
            if (deposit != null){
            totalAmount = totalAmount.add(deposit.income());
            }
        }
        return totalAmount;
    }
    public BigDecimal maxIncome()
    {
        BigDecimal maxIncome = BigDecimal.valueOf(0);
        for(Deposit deposit: deposits)
        {
            if (deposit == null)
                break;
            if (maxIncome.compareTo(deposit.income())<0)
                maxIncome = deposit.income();
        }
        return maxIncome;
    }
    public BigDecimal getIncomeByNumber(int n)
    {
        BigDecimal income = BigDecimal.valueOf(0);
        if (deposits[n] != null) {
            income = deposits[n].income();
        }
        return income;
    }
}
