package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class SpecialDepositTest {
    @DataProvider(name = "incomeTest")
    public Object[][] dataProviderMethod(){
        return new Object[][]{{BigDecimal.valueOf(30.20)}};
    }
    @Test(dataProvider ="incomeTest")
    public void testIncomeMethod(BigDecimal expected){
        SpecialDeposit specialDeposit = new SpecialDeposit(new BigDecimal(1000),2);
        var actual = specialDeposit.income();
        Assert.assertEquals(actual, expected, "The method income works incorrect");
    }
}