package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class BaseDepositTest {
    @DataProvider(name = "incomeTest")
    public Object[][] dataProviderMethod(){
        return new Object[][]{{BigDecimal.valueOf(1000).multiply(BigDecimal.valueOf(0.05))}};
    }
    @Test(dataProvider ="incomeTest")
    public void testIncomeMethod(BigDecimal expected){
        BaseDeposit baseDeposit = new BaseDeposit(new BigDecimal(1000),1);
        var actual = baseDeposit.income();
        Assert.assertEquals(actual, expected,"The method income works incorrect");
    }
}