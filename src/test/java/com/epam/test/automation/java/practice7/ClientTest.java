package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ClientTest {
    @DataProvider(name = "addDepositTest")
    public Object[][] dataProviderMethod(){
        return new Object[][]{{new LongDeposit(new BigDecimal(1000),7), true}};
    }
    @Test(dataProvider ="addDepositTest")
    public void testIncomeMethod(Deposit deposit, boolean expected){
        Client client = new Client();
        var actual = client.addDeposit(deposit);
        Assert.assertEquals(actual, expected, "The method addDeposit works incorrect");
    }
    @DataProvider(name = "maxIncomeTest")
    public Object[][] dataProviderMethod1(){
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1000),7), new SpecialDeposit(new BigDecimal(1000), 2), new BaseDeposit(new BigDecimal(1000), 1)}, new BigDecimal(150.00)}};
    }
    @Test(dataProvider ="maxIncomeTest")
    public void testMaxIncomeMethod(Deposit[] deposits,BigDecimal expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
        }
        var actual = client.maxIncome();
        Assert.assertEquals(actual, expected, "The method addDeposit works incorrect");
    }

}
