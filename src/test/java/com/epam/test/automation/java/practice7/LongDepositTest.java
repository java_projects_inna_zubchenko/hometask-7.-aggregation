package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class LongDepositTest {
    @DataProvider(name = "incomeTest")
    public Object[][] dataProviderMethod(){
        return new Object[][]{{BigDecimal.valueOf(1000).multiply(BigDecimal.valueOf(0.15))}};
    }
    @Test(dataProvider ="incomeTest")
    public void testIncomeMethod(BigDecimal expected){
        LongDeposit longDeposit = new LongDeposit(new BigDecimal(1000),7);
        var actual = longDeposit.income();
        Assert.assertEquals(actual, expected, "The method income works incorrect");
    }
}