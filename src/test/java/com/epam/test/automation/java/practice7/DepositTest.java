package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class DepositTest {
    @Test
    public void test() {
        Deposit deposit = new LongDeposit(BigDecimal.valueOf(1000), 1);
        Assert.assertEquals(deposit.getPeriod(), 1, "The method getPeriod returns incorrect value");
    }
}